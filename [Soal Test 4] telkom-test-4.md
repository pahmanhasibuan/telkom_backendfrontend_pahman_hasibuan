Apa keuntungan dan kerugian mengembangkan suatu sistem pada platform dengan non-blocking I/O?

Teknik non-blocking I/O disebut juga dengan asynchronous I/O di dalam non blocking I/O setiap request akan dilaksakan langsung tanpa harus menunggu request yang lain selesai dilaksanakan.

Keuntungan mengembangkan suatu sistem pada platform dengan non-blocking I/O :  
1.	Tanpa harus menunggu lama mendapatkan hasilnya, kerena pada non-blocking I/O setiap proses akan langsung dikembalikan, tanpa harus menunggu proses selesai.
    dan mengirim hasilnya melalui callback/listener. Contoh framework yang menggunakan non-blocking I/O adalah NodeJS

Kerugian mengembangkan suatu sistem pada platform dengan non-blocking I/O :  
1.	Agak sulit menerapkan logika terstruktur di program non-blocking code dari pada blocking I/O, karena tidak di eksekusi secara berurutan, sehingga dalam kasus ini program mesti menggunakan data untuk 
    diproses dimana data tersebut harus disimpan dalam blok yang sama untuk membuatnya dieksekusi secara berurutan.
