Menurut Anda, apa yang menyebabkan kurang lebih 90% project di industry software dikategorikan gagal (merujuk pada survey dari couchbase), baik dari segi timeline ataupun budget? Serta apa yang dapat Anda lakukan untuk meminimalisir angka tersebut?

Penyebabnya kegagalan proyek lainnya adalah : 
1. Manajemen Proyek yang Buruk
2. Tujuan proyek yang tidak realistis atau tidak terarah
3. Perkiraan sumber daya yang dibutuhkan disusun secara tidak tepat
4. Pendefinisian requirement sistem yang buruk
5. Pelaporan status proyek yang buruk
6. Komunikasi yang buruk antara pengembang dengan pelanggan

Hal yang dilakukan untuk minimalisir angka tersebut :
1.	Memperbaiki management proyek
2.	Tujuan Proyek harus realistis dan terarah
3.	Pelaporan proyek yang jujur/dan tidak menutupi progress proyek
4.	Menghindari komunikasi yang salah antara developer dengan customer
