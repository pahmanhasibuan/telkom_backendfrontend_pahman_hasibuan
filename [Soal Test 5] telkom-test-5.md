Bagaimana cara Anda melakukan unit test suatu modul yang modul tersebut mempunyai beberapa fungsi yang memerlukan koneksi database atau network call ?

Dengan metode Integration testing, bisa dilakukan dengan cara top-down atau bottom up. Pada cara top-down,   kita memerlukan stub, yaitu modul pengganti yang berperan sebagai modul yang akan ‘dipanggil’ oleh modul yang sedang diuji. Misalnya, kita 
perlu stub yang membangkitkan jam kerja secara random untuk untuk menguji modul perhitungan gaji.
Sedangkan pada cara bottom-up, kita memerlukan driver, yaitu modul pengganti yang akan ‘memanggil’ modul yang sedang diuji. Misalnya, kita perlu driver yang ‘memanggil’ fungsi hitung gaji pada modul yang sedang diuji, dengan parameter yang sesuai.
Jadi, pada struktur hirarki modul, jika kita sedang menguji modul A, maka stub adalah modul pengganti untuk modul-modul di bawah A, sedangkan driver adalah modul pengganti untuk modul-modul di atas A.


