a.	Kenapa bisa terjadi ?
Alasannya : Karena ketika loop dijalankan pertama kali, maka loop akan mencari nilai i yaitu 4, dan kemudian outputnya empat kali, yang mana satu kali untuk setiap iterasi loop. jika kita menggunakan setTimeout (), hasilnya tetap sama yaitu 

Iteration #4
Iteration #4
Iteration #4
Iteration #4

Sesuai dengan konsep “setTimeout di dalam For Loop” : Ketika loop pertama kali di jalankan maka loop akan mencari nila i (i<4)  dan akan akan ditampilkan sebanyak 4 kali dengan nilai yang sama yaitu 4, apabila tidak memakai “setTimeout” maka akan menampilkan nilai i sebanyak 4 kali secara berturut (nilai yang tidak sama)

b.	Apa yang perlu Anda lakukan untuk membenahi potongan kode tersebut, sehingga berjalan sesuai ekspektasi?
Mengubah kode tersebut dengen potongan kode dibawah ini :
for (var i = 0; i < 4; i++){
console.log(i);
}

Maka hasil yang di harapkan dari potongan kode di atas adalah

0 1 2 3
